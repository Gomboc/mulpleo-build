<header>
    <div class="menu-wrapper">
        <div class="container">
            <div class="menu-top">
                <a class="text text-bold text-white" href="#">Prescribing Information</a>
                <a class="text text-bold text-white" href="#">Summary of Product Characteristics</a>
                <a class="text text-bold text-white" href="#">Resources</a>
            </div>
        </div>
    </div>
    <div class="hero">
        <div class="container">
            <div class="menu-main">
                <div class="logo">
                    <img src="/img/mulpleo-logo-shadow-triangle_ORIG.svg"/>
                </div>
                <div>
                    <a class="title-section text-bold text-white" href="#">NICE & SMC</a>
                    <a class="title-section text-bold text-white" href="#">Mode of Action</a>
                    <a class="title-section text-bold text-white" href="#">Efficacy</a>
                    <a class="title-section text-bold text-white" href="#">Safety</a>
                    <a class="title-section text-bold text-white" href="#">Dosing</a>
                    <a class="btn btn-primary text-bold text-white" style="font-size: 14px" href="#">Request an MSL Visit</a>
                </div>
            </div>
        </div>

{{--        <img class="hero-img" src="/img/HCP-header-traffic-light.png"/>--}}
    </div>
</header>
