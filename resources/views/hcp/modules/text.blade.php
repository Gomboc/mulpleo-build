<p class="text @if($classes) {{ $classes }} @endif">
    {{ $message }}
</p>
