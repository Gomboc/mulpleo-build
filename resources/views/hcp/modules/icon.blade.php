<div class="icon">
    <img src="{{ $icon }}"/>
    <p class="text-lg">{{ $text }}</p>
</div>
