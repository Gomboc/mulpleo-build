<div class="card card-shadow card-center">
    <h2 class="title-campaign text-secondary spacer">2.5x</h2>
    <p class="text-bold text-lg spacer text-center">
        more liver disease-related
        ambulatory visits (p<0.01)
    </p>
</div>
<div class="card card-shadow card-center">
    <h2 class="title-campaign text-secondary spacer">4x</h2>
    <p class="text-bold text-lg spacer text-center">
        greater likelihood of liver
        disease-related emergency
        hospital visits (p<0.01)
        )
    </p>
</div>
<div class="card card-shadow card-center">
    <h2 class="title-campaign text-secondary spacer">13x</h2>
    <p class="text-bold text-lg spacer text-center">
        more likely to have liver diseaserelated inpatient hospital stays
        (p<0.01)
    </p>
</div>
