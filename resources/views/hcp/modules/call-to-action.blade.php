<div class="card card-secondary card-lg">
    <h2 class="spacer title-subheading {{ $messageClass }}">
        {{ $message }}
    </h2>

    @if (isset($body))
        <p class="spacer text">
            {{ $body }}
        </p>
    @endif

    <div class="spacer">
        <a class="btn {{ $btnFirstClasses }}" href="{{ $btnFirstLink }}">{{ $btnFirstMessage }}</a>
        @if(isset($btnSecondMessage))
            <a class="btn {{ $btnSecondClasses }}" href="{{ $btnSecondLink }}">{{ $btnSecondMessage }}</a>
        @endif
    </div>
</div>
