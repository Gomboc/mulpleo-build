<p class="title-subheading text-bold text-primary">
    <i>
        {{ $quote }}
    </i>
</p>
<p class="title-subheading text-bold">
    {{ $author }}
</p>
