<p class="text text-sm text-bold">
    References
</p>

@foreach($points as $key => $point)
    <p class="spacer-sm text text-sm">
        <b>{{ $key + 1 }}</b>&nbsp;
        {{ $point }}
    </p>
@endforeach
