<div x-data="{
            selectedFilter: 'Elso',
            data: {
                Elso: [{group: 'Test', pvt:1, mvt:23}, {group: 'Te2st', pvt:1, mvt:23}],
                Masodik: [{group: 'Masodik', pvt:14, mvt:234}, {group: 'Te2st444', pvt:14, mvt:243}]
            }
        }">
    <select x-model="selectedFilter">
        <option value="Elso">Elso</option>
        <option value="Masodik">Masodik</option>
    </select>

    <table class="table">
        <thead>
        <tr class="table-header">
            <th>Study</th>
            <th class="text-center" colspan="2" x-html="selectedFilter"></th>
        </tr>
        <tr>
            <th class="text">Adverse event</th>
            <th class="text">PVT†</th>
            <th class="text">MVT†</th>
        </tr>
        </thead>
        <tbody>
        <template x-for="item in data[selectedFilter]" :key="item">
            <tr>
                <td class="text" x-text="item.group">
                    Headache
                </td>
                <td class="text" x-text="item.pvt">
                    4.7 (8/1 71)
                </td>
                <td class="text" x-text="item.mvt">
                    4.7 (8/1 71)
                </td>
            </tr>
        </template>

        </tbody>
    </table>
</div>
