@extends('hcp.main-layout')

@section('content')
    <div class="page" style="background-image: url('/img/Mulpleo-Packshot-Mockup-no-BT-min.png')">
        @include('hcp.modules.page-header')

        <div class="container">
            <div class="section section-75">
                <div class="component">
                    @include('hcp.modules.text', [
                        'message' => 'Recommended by the National Institute for Health and Care Excellence (N ICE)1',
                        'classes' => 'text-bold title-section'
                    ])
                </div>
            </div>
        </div>
        <div class="container">
            <div class="section section-75">
                <div class="component">
                    @include('hcp.modules.text', [
                        'message' => 'NICE recommends Mulpleo for the treatment of severe thrombocytopenia in adults with chronic liver disease (CLD)needing a planned invasive procedure, making the following points in support of its decision:',
                        'classes' => 'text-bold title-section'
                    ])
                </div>
            </div>
        </div>

        <div class="container">
            <div class="section section-75">
                <div class="component">
                    @include('hcp.modules.accordion')
                </div>
            </div>
        </div>

        <div class="container">
            <div class="section section-75">
                <div class="component">
                    @include('hcp.modules.table')
                </div>
            </div>
        </div>

        <div class="container">
            <div class="section section-75">
                <div class="component">
                    @include('hcp.modules.filter-list')
                </div>
            </div>
        </div>


        <div class="container">
            <div class="section section-75">
                <div class="component">
                    @include('hcp.modules.icon-list', [
                        'message' => 'NICE recommends Mulpleo for the treatment of severe thrombocytopenia in adults with chronic liver disease (CLD)needing a planned invasive procedure, making the following points in support of its decision:',
                        'classes' => 'text-bold title-section'
                    ])
                </div>
            </div>
        </div>

        <div class="container">
            <div class="section">
                <div class="component">
                    <div class="grid grid-2">
                        @include('hcp.modules.call-to-action', [
                              'message' => 'Mode of Action',
                              'messageClass' => 'title-subheading text-bold text-secondary',
                              'body' => 'title-subheading text-bold text-secondary',
                              'btnFirstMessage' => 'Read more',
                              'btnFirstLink' => '#',
                              'btnFirstClasses' => 'btn-simpler',
                          ])

                        @include('hcp.modules.call-to-action', [
                         'message' => 'Mode of Action',
                         'messageClass' => 'title-subheading text-bold text-secondary',
                         'body' => 'title-subheading text-bold text-secondary',
                         'btnFirstMessage' => 'Read more',
                         'btnFirstLink' => '#',
                         'btnFirstClasses' => 'btn-simpler',
                     ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
