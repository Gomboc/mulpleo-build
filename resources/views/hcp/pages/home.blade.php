@extends('hcp.main-layout')

@section('content')
    <div class="container">
        <div class="section section-75">
            <div class="component">
                @include('hcp.modules.text', [
                    'message' => 'Thrombocytopenia in chronic liver disease (CLD) patients can result in complications in patient care, often leading to delayed or cancelled procedures 2,3',
                    'classes' => 'text-bold'
                ])
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section section-75">
            <div class="component">
                @include('hcp.modules.text', [
                    'message' => 'Estimates suggest that the costs of routine invasive procedures in patients with CLD are 3.5x higher in patients with severe thrombocytopenia vs. non-thrombocytopenic patients.4 Compared to non-thrombocytopenic CLD patients,patients with CLD and a platelet count of ≤100x109/L experience:3',
                    'classes' => ''
                ])
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="component">
                <div class="grid grid-3">
                    @include('hcp.modules.cards')
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section section-75">
            <div class="component">
                @include('hcp.modules.quote', [
                    'quote' => '“Obtaining, storing and administering platelets carries a number of practical implications for patients and for service delivery”',
                    'author' => '– National Institute for Health and Care Excellence (NICE),2020'
                 ])
            </div>
        </div>

        <div class="container">
            <div class="section section-75">
                <div class="component">
                    @include('hcp.modules.text', [
                        'message' => 'Mulpleo is the first thrombopoietin (TPO) receptor agonist approved for the treatment of severe thrombocytopenia in adult patients with chronic liver disease undergoing invasive procedures.',
                        'classes' => 'text-lg'
                    ])
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="component">
                <div class="grid grid-3">
                    @include('hcp.modules.icon', [
                        'icon' => '/img/tick-label.svg',
                        'text' => 'NICE and SMC approved5,6'
                    ])

                    @include('hcp.modules.icon', [
                      'icon' => '/img/platlets.svg',
                      'text' => 'TPO receptor agonist1'
                  ])

                    @include('hcp.modules.icon', [
                      'icon' => '/img/single-pill-updated.svg',
                      'text' => 'Oral tablet1'
                  ])

                    @include('hcp.modules.icon', [
                      'icon' => '/img/calendar-7-days.svg',
                      'text' => 'Once-daily for 7 days1'
                  ])

                    @include('hcp.modules.icon', [
                      'icon' => '/img/plate-fork-knife.svg',
                      'text' => 'With or without food'
                  ])

                    @include('hcp.modules.icon', [
                      'icon' => '/img/day-9-calendar.svg',
                      'text' => 'Procedure performed from day 9 after the start of Mulpleo treatment1*'
                  ])
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="component">
                <div class="grid">
                    @include('hcp.modules.text', [
                           'message' => '*Platelet count should be measured prior to a procedure.1',
                           'classes' => 'text-sm'
                       ])
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="component">
                <div class="grid grid-2">
                    @include('hcp.modules.text', [
                          'message' => 'With Mulpleo, severe thrombocytopenia doesn’t have to stand in the way of performing your procedures in adult patients with chronic liver disease (CLD)',
                          'classes' => 'title-subheading text-bold text-secondary text-v-center'
                      ])

                    @include('hcp.modules.image', [
                          'image' => '/img/Mulpleo-Packshot-Mockup-no-BT-min.png'
                      ])
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section section-75">
            <div class="component">
                @include('hcp.modules.call-to-action', [
                      'message' => 'Help support your patients in the appropriate use of Mulpleo with our specially designed patient resources and patient website',
                      'messageClass' => 'title-subheading text-bold text-primary',
                      'btnFirstMessage' => 'Visit the Make It Count patient website',
                      'btnFirstLink' => '#',
                      'btnFirstClasses' => 'btn-carbon',
                      'btnSecondMessage' => 'View resource',
                      'btnSecondLink' => '#',
                      'btnSecondClasses' => 'btn-link',
                  ])
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section section-75">
            <div class="component">
                @include('hcp.modules.reporting', [
                      'message' => 'This medicinal product is subject to additional monitoring. This will allow quick identification of new safety information. Adverse events should be reported. Reporting forms and information can be found at yellowcard.mhra.gov.uk. Adverse events should also be reported to Shionogi on Tel: 02030534190 or via contact@shionogi.eu'
                  ])
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section section-75">
            <div class="component">
                @include('hcp.modules.references', [
                      'points' => [
                          'Mulpleo (lusutrombopag) Summary of Product Characteristics.',
                          'National Institute for Health and Care Excellence (NICE). Lusutrombopag for treating thrombocytopenia in people with chronic liver disease needing a planned invasive procedure. TA617. January 2020. Available at: https://www.nice.org.uk/guidance/ta617.(Accessed May 2020).',
                          'Mulpleo (lusutrombopag) Summary of Product Characteristics.',
                          'Mulpleo (lusutrombopag) Summary of Product Characteristics.',
                          'Mulpleo (lusutrombopag) Summary of Product Characteristics.'
]
                  ])
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section section-75">
            <div class="component">
                @include('hcp.modules.text', [
                      'message' => 'PP-UK-LUS-0084. Date of preparation: May 2020',
                      'classes' => 'text-sm'
                      ])
            </div>
        </div>
    </div>

@endsection
